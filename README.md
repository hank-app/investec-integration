# Investec Integration

How to get your programmable banking talking to Hank App. For more info on what we are up to visit our [website](https://www.hankapp.co.za).

## Instructions

Log into your Investec profile and navigate to Programmable Banking for the card you want to enable

Open up the env.json file under the Files section. Paste the following json replacing `{{APIKEY}}` with the API Key you received in the registration email.

```json
{
    "hankTransactionsUrl": "https://us-central1-hank-app.cloudfunctions.net/transactions/afterTransaction",
    "hankApiKey": "{{APIKEY}}"
}
```

![env.json](images/env.png)

In the main.js file add the `postAfterTransaction` function and await it in the `afterTransaction` hook as shown below.

```js
async function postAfterTransaction(transaction) {
    const response = await fetch(process.env.hankTransactionsUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': process.env.hankApiKey
        },
        body: JSON.stringify(transaction)
    });
};

// This function runs after a transaction.
const afterTransaction = async (transaction) => {
    console.log(transaction);
    await postAfterTransaction(transaction);
};
```

![main.js](images/main.png)

Press `CTRL + S` or `Command + S` to save and click `Deploy code to card` in the bottom right.

You are done. Test a transaction.

## Issues

If you have any issues, please feel free to log an issue in this repo or contact us at [info@hankapp.co.za](mailto:info@hankapp.co.za).
