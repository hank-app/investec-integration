// This function runs during the card transaction authorization flow.
// It has a limited execution time, so keep any code short-running.
const beforeTransaction = async (authorization) => {
    console.log(authorization);
    return true;
};

async function postAfterTransaction(transaction) {
    const response = await fetch(process.env.hankTransactionsUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': process.env.hankApiKey
        },
        body: JSON.stringify(transaction)
    });
};

// This function runs after a transaction.
const afterTransaction = async (transaction) => {
    console.log(transaction);
    await postAfterTransaction(transaction);
};